import React from 'react';
import { Link } from 'react-router-dom';
import { PostData } from '../public/PostData.js';
import { Redirect } from 'react-router-dom';

const Landing = React.createClass({
  getInitialState: function() {
    return {
      username: '',
      password: '',
      redirectFlag: false // As it's initially not logged-in, redirection to "Landing" is allowed
    };
  },
  login: function() {
    if(this.state.username && this.state.password) {
      PostData('login', this.state).then((result) => { // Pass the current state to the PostData "fetch function" at /public/PostData.js
        let responseJSON = result;
        if(responseJSON.userId) { // Validates that the responseJSON has been successfully arrived
          sessionStorage.setItem('userData', responseJSON); // Stores the user data into session storage
          this.setState({ redirectFlag: true }); // To prevent redirection to "Landing/Login" while logged-in
        }
        else {
          alert("Sorry, incorrect data or user not found!");
        }
      });
    }
    else {
      alert("Please, enter your username and password to login!")
    }
  },
  onInputChange: function(evt) {
    this.setState({ [evt.target.name]: evt.target.value }); // evt.target.name -> to specify which input we are in, and then change its state
  },
  render: function () {
    if(sessionStorage.getItem("userData")) { // If session storage has "userData"..
      return <Redirect  to={'/search'} />; // Automatically redirect to Search Component
    }
    if(this.state.redirectFlag) { // If redirectFlag is true ..
      return <Redirect to={'/search'} /> // Redirect to Search Component when trying to access "Landing"
    }
    return (
      <div className='landing'>
        <h1>Reapp - React.js Applications</h1>
        <input type='text' className="username" name="username" placeholder='Insert username ..' onChange={this.onInputChange} />
        <input type='password' className="password" name="password" placeholder='Insert password ..' onChange={this.onInputChange} />
        <button className="login-button-style" onClick={this.login}>Sign-In</button><br /><span>Don't have an account? <a href="/register">Register Now</a></span>
      </div>
    );
  }
});

export default Landing;
