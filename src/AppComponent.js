import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Landing from './Landing';
import Search from './Search';
import Details from './Details';
import Register from './Register';
import LoginToView from './LoginToView';
import NotFoundPage from './NotFoundPage';
import Logout from './Logout';
import preload from '../public/data.json';
import 'whatwg-fetch';
import '../public/style.css';

var AppComponent = React.createClass({
  render: function () {
    return (
      <BrowserRouter>
        <div className='app'>
          <Switch>
            <Route exact path="/" component={Landing}/>
            <Route path="/register" component={Register}/>
            <Route path="/LoginToView" component={LoginToView}/>
            <Route path="/logout" component={Logout} />
            <Route
              path='/search'
              component={(props) => <Search shows={preload.shows} {...props} />}
            />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
});

render(<AppComponent />, document.getElementById('react-app'));
