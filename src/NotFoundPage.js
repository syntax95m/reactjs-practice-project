import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = React.createClass({
    render: function() {
        return (
            <div className="landing">
                <h1>404 - Page Not Found.</h1>
                <Link to="/">Home Page</Link>
            </div>
        );
    }
});

export default NotFoundPage;