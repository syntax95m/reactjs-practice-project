/* Unit Testing using Jest */
import React from 'react';
import renderer from 'react-test-renderer'; // Provided by Facebook for unit testing purposes
import Search from '../Search';

test('Search renders correctly', () => {
  const component = renderer.create(<Search />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
