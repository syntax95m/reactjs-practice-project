import React from 'react';
import '../public/style.css';

const LoginToView = React.createClass({
    render: function() {
        return (
            <div className="landing">
                <h1>Sorry, you need to login first before viewing this page!</h1>
                <a href="/">Login Now</a>
            </div>
        );
    }
});

export default LoginToView;