import React from 'react';
import { Link } from 'react-router-dom';

const Logout = React.createClass({
    render: function() {
        return (
            <div className="landing">
                <h1>You've been logged out.</h1>
                <Link to="/">Home Page</Link>
            </div>
        );
    }
});

export default Logout;